<?php

function createDir($dir)
{
    if ( ! is_dir($dir) ) {
        mkdir($dir, 0755, true);
    }
    
    return;
}

function deleteDir($dir)
{
    if ( is_dir($dir) ) {
        // Get the files in the folder
        $files = scandir( $dir );

        // Remove the two first results '.' and '..'
        unset($files[0]);
        unset($files[1]);

        // Loop to delete all files
        foreach ($files as $file) {
            if ( is_file($dir . $file) ) {
                unlink($dir . $file);
            }
        }

        // Delete the directory and return
        return rmdir($dir);
    }
    
    return;
}

function sanitizeMoney($value) {
    $value = str_replace('R$', '', $value);
    $value = str_replace(' ',  '', $value);
    $value = str_replace(',',  '', $value);
    $value = str_replace('.',  '', $value);
    $value = $value / 100;
    
    return $value;
}

function sanitizeCPF($cpf) {
    $cpf = str_replace('.', '', $cpf);
    $cpf = str_replace('-', '', $cpf);
    $cpf = str_replace('/', '', $cpf);
    return $cpf;
}

function transformToMoney($value) {
    $value = 'R$ ' . number_format($value, 2, ',', '.');
    return $value;
}

function removeEmptyValuesFromArray($values)
{
    $i = 0;
    
    foreach ($values as $value) {
        if ( empty($value) ) {
            unset($values[$i]);
        }
        $i++;
    }
    
    return $values;
}

function sanitizeTel($tel) {
    $tel = str_replace('(', '', $tel);
    $tel = str_replace(')', '', $tel);
    $tel = str_replace(' ', '', $tel);
    $tel = str_replace('-', '', $tel);
    return $tel;
}

/**
 * Retorna o DDD de um número de telefone
 * @param string $tel
 * @return string
 */
function getDDD($tel) {
    $tel = sanitizeTel($tel);
    return substr($tel, 0, 2);
}

/**
 * Retorna número de telefone sem o DDD
 * @param string $tel Número de telefone
 * @param string $type 'tel' : 'cel'
 * @return string
 */
function getNumber($tel, $type) {
    $digits = ($type == 'cel') ? 9 : 8;
    $tel = sanitizeTel($tel);
    return substr($tel, 2, $digits);
}

function arrayToJson($data) {
    return json_encode($data);
}

function jsonToArray($data) {
    return json_decode($data, true);
}

function jsonToObject($data) {
    return json_decode($data);
}