<?php

namespace App;

use PagarMe\Sdk\Balance\Request\BalanceGet;
use PagarMe\Sdk\RequestInterface;
use PagarMe\Sdk\PagarMe as Pagar;
use App\Configuration as Config;
use GuzzleHttp\Client;
use GuzzleHttp\Exception;
use Carbon\Carbon;
use stdClass;

class Pagarme
{

    public $mode;
    public $api_key;
    public $pagarme;

    public function __construct()
    {
        $this->mode = Config::where('key', 'pagarme_mode')->first()->value;
        $this->api_key = Config::where('key', 'pagarme_'.$this->mode.'_api_key')->first()->value;
        $this->pagarme = new Pagar( $this->api_key );
    }
    
    public function saldo()
    {
        while ( !isset($money) ) {
            $money = $this->pagarme->balance()->get();
        }
        
        return $money;
    }
    
    /**
     * Retorna um array com todas as transações
     * com paginação.
     * @param int $page
     * @param int $count
     * @return array
     */
    public function transacoes($page = 1, $count = 10)
    {
        do {
            $transactions = $this->pagarme->transaction()->getList($page, $count);
        } while ( !isset($transactions) );

        $transactionList = collect([]);

        foreach ( $transactions as $transaction ) {
            
            $operation = new stdClass;
            $operation->dataOperacao    = $transaction->dateCreated->format('d/m/Y H:i:s');
            $operation->metodoPagamento = $transaction->paymentMethod;
            $operation->valorBruto      = 'R$ ' . number_format(($transaction->amount / 100), 2, ',', '.');
            $operation->valorPago       = 'R$ ' . number_format(($transaction->paidAmount / 100), 2, ',', '.');
            $operation->custo           = 'R$ ' . number_format(($transaction->cost / 100), 2, ',', '.');
            $operation->valorLiquido    = 'R$ ' . number_format(($transaction->paidAmount / 100 - $transaction->cost / 100), 2, ',', '.');
            $operation->status          = $transaction->status;
            $operation->ip              = $transaction->ip;
            
            if ( $transaction->paymentMethod == 'credit_card' ) {
                $operation->cartao = new stdClass;
                $operation->cartao->id             = $transaction->card->id;
                $operation->cartao->nome           = $transaction->card->holder_name;
                $operation->cartao->bandeira       = $transaction->card->brand;
                $operation->cartao->pais           = $transaction->card->country;
                $operation->cartao->ultimosDigitos = $transaction->card->last_digits;
            }
            
            $transactionList->push( $operation );
        }

        return $transactionList;
    }
    
    public function transacao($id)
    {
        do {
            $transaction = $this->pagarme->transaction()->get($id);
        } while ( !isset($transaction) );
        
        return $transaction;
    }
}
