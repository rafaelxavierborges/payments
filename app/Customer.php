<?php

namespace App;

use App\Customer;
use PagarMe\Sdk\Customer\Customer as PagarmeCustomer;
use stdClass;
use DB;

class Customer
{
    public $customer;
    public $config;
    
    public function __construct()
    {
        $this->config = Configuration::getConfigs('pagarme');
        $this->apikey = $this->config->{$this->config->mode . "_api_key"};
    }
    
    /**
     * Cria todos os dados da compra e do cliente
     * para transformar em JSON e enviar a requisição
     * via cURL.
     * 
     * @param Array $data
     * @return Object $customer
     */
    public function create($data)
    {
        
        $document_type = 'cpf';
        
        $this->customer = new stdClass;
        
        // Set keys
        $this->customer->api_key = $this->apikey;
        $this->customer->payment_method = $data['payment_method'];
        $this->customer->amount = $data['amount'];
        
        // Init all objects
        $this->customer->customer = new stdClass;
        $this->customer->billing  = new stdClass;
        $this->customer->shipping = new stdClass;
        $this->customer->items    = new stdClass;
        
        // Customer personal data and docs
        $this->customer->customer->external_id     = '';
        $this->customer->customer->name            = $data['customer_name'];
        $this->customer->customer->type            = 'individual';
        $this->customer->customer->country         = 'br';
        $this->customer->customer->email           = $data['customer_email'];
        $this->customer->customer->document_type   = $document_type;
        $this->customer->customer->document_number = $data['customer_cpf'];
        
        // Customer docs
        $document = new stdClass;
        $document->type   = $document_type;
        $document->number = $data['customer_cpf'];
        
        // Customer docs, phone and birthday
        $this->customer->customer->documents     = array($document);
        $this->customer->customer->phone_numbers = array($data['customer_cel']);
        $this->customer->customer->birthday      = '';
        
        // Customer billing info
        $this->customer->billing->name                    = $data['customer_billing']['name'];
        $this->customer->billing->address                 = new stdClass;
        $this->customer->billing->address->country        = 'br';
        $this->customer->billing->address->state          = $data['customer_billing']['state'];
        $this->customer->billing->address->city           = $data['customer_billing']['city'];
        $this->customer->billing->address->neighborhood   = $data['customer_billing']['neighborhood'];
        $this->customer->billing->address->street         = $data['customer_billing']['street'];
        $this->customer->billing->address->street_number  = $data['customer_billing']['street_number'];
        $this->customer->billing->address->zipcode        = $data['customer_billing']['zipcode'];
        $this->customer->billing->address->complementary  = $data['customer_billing']['complementary'];
        
        // Customer shipping info
        $this->customer->shipping->name                   = $data['customer_shipping']['name'];
        $this->customer->shipping->fee                    =  0;
        $this->customer->shipping->delivery_date          = '';
        $this->customer->shipping->expedited              = false;
        $this->customer->shipping->address                = new stdClass;
        $this->customer->shipping->address->country       = 'br';
        $this->customer->shipping->address->state         = $data['customer_shipping']['state'];
        $this->customer->shipping->address->city          = $data['customer_shipping']['city'];
        $this->customer->shipping->address->neighborhood  = $data['customer_shipping']['neighborhood'];
        $this->customer->shipping->address->street        = $data['customer_shipping']['street'];
        $this->customer->shipping->address->street_number = $data['customer_shipping']['street_number'];
        $this->customer->shipping->address->zipcode       = $data['customer_shipping']['zipcode'];
        $this->customer->shipping->address->complementary = $data['customer_shipping']['complementary'];
        
        // Items
        $this->customer->items = $data['items'];
        
        return $this->customer;
    }
    
    /**
     * Cadastra um novo cliente no sistema.
     * @param array $data Array de dados da compra e do cliente
     * @param Illuminate\Database\Eloquent $transaction Objeto registro da transação no DB
     */
    public static function store($data, $transaction)
    {
        
        $address  = $data->billing->address->street . ', ';
        $address .= $data->billing->address->street_number;
        
        $customer = array(
            'name'         => $data->customer->name,
            'doctype'      => $data->customer->document_type,
            'docnum'       => $data->customer->document_number,
            'address'      => $address,
            'neighborhood' => $data->billing->address->neighborhood,
            'city'         => $data->billing->address->city,
            'state'        => $data->billing->address->state,
            'zipcode'      => $data->billing->address->zipcode,
            'complement'   => $data->billing->address->complementary,
        );
        
        $inserted = DB::table('customers')->insert( $customer );
        
        if ( $inserted ) {
            $id = DB::getPdo()->lastInsertId();
            $created = DB::table('customers')->find($id);
            return $created;
        } else {
            throw new Exception("Erro ao inserir novo cliente na tabela 'customers'. - App/Customer (Linha 122)");
        }
    }
}
