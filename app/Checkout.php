<?php

namespace App;

use App\HttpRequest;
use App\Transaction;
use App\Customer;
use App\Sale;

class Checkout
{
    public function transaction($create)
    {
        $url = 'https://api.pagar.me/1/transactions';
        $response = HttpRequest::jsonPost($url, $create);
        
        $no_errors = ! strpos($response, 'error');
        
        if ( $no_errors ) {
            $data = jsonToObject($response);
            $transaction = Transaction::store($data);
            $customer = Customer::store($create, $transaction);
            $sale = Sale::store($transaction, $customer, $data);
            return true;
        } else {
            return false;
        }
    }
}