<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }
    
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name'        => 'string|min:3|max:255|required',
            'code'        => 'string|max:255|required|unique:products',
            'value'       => 'required',
            'description' => 'text',
            'imagetop'    => 'file|required',
            'imageright'  => 'file|required',
            'imagecover'  => 'file',
        ];
    }
    
    /**
    * Get the error messages for the defined validation rules.
    *
    * @return array
    */
   public function messages()
   {
       return [
           'name.required' => 'Digite o nome do produto.',
           'name.min'      => 'O nome do produto deve ter pelo menos 3 caracteres.',
           'name.max'      => 'O nome do produto deve ter no máximo 255 caracteres.',
           'name.string'   => 'O nome do produto deve ser uma string.',
           'code.required' => 'Digite o código do produto.',
           'code.string'   => 'O código do produto deve ser uma string.',
           'code.max'      => 'O código do produto deve ter no máximo 255 caracteres.',
           'description.required' => 'Digite a descrição do produto.',
           'description.string'   => 'A descrição do produto deve ser uma string.',
           'imagetop.file'        => 'O banner do topo não é um arquivo válido.',
           'imagetop.required'    => 'Selecione uma imagem para o banner do topo.',
           'imageright.file'      => 'A imagem da lateral direita não é um arquivo válido.',
           'imagetop.required'    => 'Selecione uma imagem para exibir na lateral direita.',
           'imagecover.file'      => 'A imagem de capa não é um arquivo válido.'
       ];
   }
}
