<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $input = $this->all();
        $user_id = (!empty($input['id'])) ? $input['id'] : '';
        
        
        if ( $this->method() == 'POST' ) {
            // Create new
            return [
                'name'  => 'required|min:3|max:255',
                'email' => 'required|email|unique:users',   
                'password' => 'required'
            ];
        } else {
            // Update
            return [
                'name'  => 'required|min:3|max:255',
                'email' => 'unique:users,email,'.$user_id,  // email unico, exceto o proprio email
                'password' => 'required'
            ];
        }
    }
    
    public function messages()
    {
        return [
            'name.required'     => 'Preencha o nome',
            'name.min'          => 'Nome deve conter pelo menos 3 caracteres',
            'name.max'          => 'Nome deve conter no máximo 255 caracteres',
            'email.required'    => 'Preencha o email',
            'email.email'       => 'Email inválido',
            'email.unique'      => 'Este email já está em uso',
            'password.required' => 'Preencha a senha',
        ];
    }
}
