<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\CheckoutRequest;
use App\Product;
use App\Customer;
use App\Checkout;
use App\Configuration;
use PagarMe\Sdk\PagarMe;
use PagarMe\Sdk\Customer\Customer as PagarmeCustomer;
use stdClass;

class CheckoutController extends Controller
{   
    public $checkout;
    public $product;
    public $customer;
    public $config;
    public $pagarme;
    public $apikey;
    public $encryptionkey;
    
    public function __construct() {
        $this->config = Configuration::getConfigs('pagarme');
        $this->apikey = $this->config->{$this->config->mode . "_api_key"};
        $this->encryptionkey = $this->config->{$this->config->mode . "_encryption_key"};
        $this->checkout = new Checkout;
        $this->product  = new Product;
        $this->customer = new Customer;
        $this->pagarme  = new PagarMe( $this->apikey );
    }
    
    public function product($product_code)
    {
        // Get product
        $product = Product::where('code', $product_code)->first();
        //dd($product);
        // If didn't find ABORT
        if ( ! $product ) {
            abort(404);
        }
        
        // Get this year
        $year = (int) date('y');
        $limit = $year + 10;
        
        // Get array with next 10 years
        $years = array();
        for ($i = $year; $i <= $limit; $i++) {
            array_push($years, $i);
        }
        
        // Parcelamento & Juros
        $parcelas = $product->getParcelas();
        
        return view('product.checkout', compact('product', 'parcelas', 'years'));
    }
    
    public function transaction($request, $product)
    {
        
        $item             = new stdClass;
        $item->id         = $product->id;
        $item->title      = $product->name;
        $item->unit_price = round(sanitizeMoney($product->value) * 100, 2);
        $item->quantity   = 1;
        $item->tangible   = ($product->physical_product) ? 1 : 0;
        
        $data = array(
            'amount'         => $item->unit_price,
            'payment_method' => $request->payment_method,
            'installments'   => 1,
            'customer_name'  => $request->nome,
            'customer_email' => $request->email,
            'customer_cpf'   => sanitizeCPF($request->cpf),
            'customer_cel'   => $request->celular
        );
        
        if ( $request->payment_method == 'credit_card' ) {
            $data['installments'] = $request->parcelas;
        }
        
        $shipping = array(
            'name'          => $request->nome,
            'state'         => $request->estado,
            'city'          => $request->cidade,
            'neighborhood'  => $request->bairro,
            'street'        => $request->endereco,
            'street_number' => $request->numero,
            'zipcode'       => $request->cep,
            'complementary' => $request->complemento
        );
        
        $data['customer_shipping'] = $shipping;
        
        if ($request->has('address_is_the_same')) {
            $data['customer_billing'] = $shipping;
        } else {
            $data['customer_billing'] = array(
                'name'          => $request->cobranca_nome,
                'state'         => $request->cobranca_estado,
                'city'          => $request->cobranca_cidade,
                'neighborhood'  => $request->cobranca_bairro,
                'street'        => $request->cobranca_endereco,
                'street_number' => $request->cobranca_numero,
                'zipcode'       => $request->cobranca_cep,
                'complementary' => $request->cobranca_complemento
            );
        }
        
        $data['items'] = array( $item );
        
        $create = $this->customer->create($data);
        
        return $this->checkout->transaction($create);
    }
    
    public function checkout(CheckoutRequest $request, $product_code)
    {
        $product = $this->product->where('code', $product_code)->first();
        return $this->transaction($request, $product);
        //return $this->boleto($request, $product);
    }
    
    public function customer($request)
    {
        $customer = [
            'name'  => $request->nome,
            'email' => $request->email,
            'document_number' => sanitizeCPF( $request->cpf ),
            'documentType' => 'cpf',
            'address' => [
                'street'        => $request->endereco,
                'street_number' => $request->numero,
                'neighborhood'  => $request->bairro,
                'zipcode'       => $request->cep,
                'complementary' => $request->complemento,
                'city'          => $request->cidade,
                'state'         => $request->estado,
                'country'       => 'Brasil'
            ],
            'phone' => [
                'ddd'    => getDDD($request->celular),
                'number' => getNumber($request->celular, 'cel')
            ],
        ];
        
        $created = new PagarmeCustomer($customer);
        
        return $created;
    }
    
    public function boleto($request, $product)
    {
        $price = (int) (sanitizeMoney($product->value) * 100);
	$postbackUrl = $this->config->postback;
	$metadata = [
            'idProduto'  => $product->id,
            'codProduto' => $product->code
        ];
	$customer = $this->customer($request);
        //dd([$price, $customer, $postbackUrl, $metadata]);
        
        return $this->pagarme->transaction()->boletoTransaction(
            $price,
            $customer,
            $metadata
        );
    }
}
