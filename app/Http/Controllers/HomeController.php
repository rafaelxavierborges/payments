<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pagarme;
use Exception;

class HomeController extends Controller
{
    
    protected $pagarme;
    
    public function __construct()
    {
        $this->middleware('auth');
        $this->pagarme = new Pagarme;
    }

    /**
     * Show application's dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        try {
            $error = '';
            @ $money = $this->pagarme->saldo();
            @ $transactions = $this->pagarme->transacoes();
        } catch (Exception $e) {
            $money = 0;
            $trasactions = array();
            $error = $e->getMessage();
        }

        return view('dashboard', compact('money', 'transactions', 'error'));
    }
}
