<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ProductRequest;
use App\Product;
use Exception;
use Redirect;
use Session;
use DB;

class ProductController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $products = Product::orderBy('name')->get();
        return view('product.index', compact('products'));
    }

    public function create()
    {
        return view('product.create');
    }

    public function store(ProductRequest $request)
    {
        DB::beginTransaction();
        
        try {
            $input = $request->except(['_token', 'imagetop', 'imagecover', 'imageright']);
            
            $files = collect([
                'imagetop'   => $request->file('imagetop'),
                'imagecover' => $request->file('imagecover'),
                'imageright' => $request->file('imageright')
            ]);
            
            $input['value'] = sanitizeMoney($input['value']);
            
            if ( !empty($input['postbacks']) ) {
                $input['postbacks'] = json_encode($input['postbacks']);
            }
            
            $created = DB::table('products')->insert( $input );
            
            if ( $created ) {
                $id = DB::getPdo()->lastInsertId();
                $created = Product::find($id);
            }
            
            $uploaded = Product::upload($files, $created->id);

            if ( $created && $uploaded ) {
                Session::flash('success', 'Produto cadastrado.');
                DB::commit();
            } else {
                Session::flash('error', 'Erro ao cadastrar produto.');
                DB::rollback();
            }
        
            return Redirect::route('product.index');
        } catch (Exception $e) {
            DB::rollback();
            $message = "{$e->getFile()} ({$e->getLine()}): {$e->getMessage()}";
            Session::flash('error', $message);
            return Redirect::back()->withInput();
        }
    }

    public function edit($id)
    {
        //
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        $deleted = Product::deleteThis($id);
        
        if ( $deleted ) {
            Session::flash('success', 'Produto excluído.');
        } else {
            Session::flash('error', 'Erro ao excluir produto.');
        }
        
        return Redirect::route('product.index');
    }
}
