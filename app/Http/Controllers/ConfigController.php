<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Configuration as Config;
use Redirect;
use Session;
use stdClass;

class ConfigController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function editPagarme()
    {
        $pagarme = Config::getConfigs('pagarme');
        return view('config.pagarme', compact('pagarme'));
    }
    
    public function updateConfigs($request)
    {
        if ( $request->has('product_image_dir') ) {
            $directory = $request->product_image_dir;
            createDir( $directory );
        }
        
        if ( $request->has('pagarme_postpacks') ) {
            $this->updateDefaultPostbacks( $request->pagarme_postpacks );
        }
        
        $these_fields = ['_method', '_token', 'pagarme_postpacks'];
        $input = $request->except($these_fields);
        
        foreach ( $input as $key => $value ) {
            $config = Config::where('key', $key)->first();
            $config->value = $value;
            $config->save();
        }
        
        return;
    }
    
    public function updatePagarme(Request $request)
    {
        $this->updateConfigs($request);
        Session::flash('success', 'Configurações atualizadas');
        return Redirect::route('config.pagarme.edit');
    }
    
    public function updateDefaultPostbacks($postbacks)
    {
        $postbacks = removeEmptyValuesFromArray($postbacks);
        $postbacks = collect($postbacks)->toJson();
        $config = Config::where('key', 'default_postbacks')->first();
        return $config->update(['value' => $postbacks]);
    }
    
    public function configProduct()
    {
        $product = Config::getConfigs('product');   
        return view('config.product', compact('product'));
    }
    
    public function updateProduct(Request $request)
    {
        $this->updateConfigs($request);
        Session::flash('success', 'Configurações atualizadas');
        return Redirect::route('config.product.edit');
    }
}
