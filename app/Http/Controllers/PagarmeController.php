<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Pagarme;
use App\Configuration as Config;

class PagarmeController extends Controller
{
 
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function getSaldo()
    {
        $pagarMe = new Pagarme;
        $money = $pagarMe->saldo();

        return view('pagarme.saldo', compact('money'));
    }
}
