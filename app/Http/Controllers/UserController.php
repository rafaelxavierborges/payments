<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\UserRequest;
use App\User;
use Redirect;
use Session;

class UserController extends Controller
{
    
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function index()
    {
        $users = User::all();
        return view('user.index', compact('users'));
    }

    public function create()
    {
        return view('user.create');
    }

    public function store(UserRequest $request)
    {
        $input = $request->all();
        $created = User::create( $input );
        if ( $created ) {
            Session::flash('success', 'Usuário cadastrado');
        } else {
            Session::flash('success', 'Usuário não cadastrado');
        }
        return Redirect::route('user.index');
    }

    public function edit($id)
    {
        $user = User::find($id);
        return view('user.edit', compact('user'));
    }

    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $input = $request->except('password');
        if  ( !empty($request->password) ) {
            $input['password'] = $request->password;
        }
        $updated = $user->update( $input );
        if ( $updated ) {
            Session::flash('success', 'Usuário atualizado');
        } else {
            Session::flash('success', 'Usuário não atualizado');
        }
        return Redirect::route('user.index');
    }

    public function destroy($id)
    {
        $deleted = User::destroy($id);
        if ( $deleted ) {
            Session::flash('success', 'Usuário excluído');
        }
        return Redirect::route('user.index');
    }
}
