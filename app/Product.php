<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Configuration as Config;
use Image;

class Product extends Model
{
    protected $fillable = ['name', 'code', 'value', 'description', 'physical_product', 'max_parcelas', 'porcentagem_juros_parcelado_mes'];
    public $config;
    
    public function __construct() {
        $this->config = Config::getConfigs('product');
    }
    
    /**
     * 
     * @return Array Parcelas e valores
     */
    public function getParcelas()
    {
        // Create an empty array
        $parcelas = array();
        
        // Get and sanitize value
        $value = sanitizeMoney($this->value);
        
        // Juros
        $taxa_juros = $this->porcentagem_juros_parcelado_mes;
        $juros = round($value * ($taxa_juros / 100), 2);
        
        for ( $i = 1; $i <= $this->max_parcelas; $i++ ) {
            $valor = round($value / $i, 2, PHP_ROUND_HALF_DOWN) + (($i - 1) * $juros);
            
            $parcelas[] = array(
                'quantidade' => $i,
                'valor'      => $valor,
                'descricao'  => "{$i}x de ".transformToMoney($valor)
            );
        }
        
        return $parcelas;
    }
    
    public function getImagetopAttribute($value)
    {
        return '/' . $this->config->product_image_dir . '/' . $this->id . '/imagetop.png';
    }
    
    public function getImagerightAttribute($value)
    {
        return '/' . $this->config->product_image_dir . '/' . $this->id . '/imageright.png';
    }
    
    public function getImagecoverAttribute($value)
    {
        return '/' . $this->config->product_image_dir . '/' . $this->id . '/imagecover.png';
    }
    
    public function getValueAttribute($value)
    {
        return 'R$ ' . number_format($value, 2, ',', '.');
    }
    
    public function getPostbacksAttribute($value)
    {
        $postbacks = collect(json_decode($value, true));
        return $postbacks;
    }
    
    public function setValueAttribute($value)
    {
        $value = str_replace('R$', '', $value);
        $value = str_replace(' ',  '', $value);
        $value = str_replace(',',  '', $value);
        $value = str_replace('.',  '', $value);
        $value = $value / 100;
        $this->attributes['value'] = $value;
    }
    
    public static function upload($files, $product_id)
    {
        // Starts with true but if something goes wrong it becomes false
        $uploaded = true;
        
        // Get configs about size of images
        $config = Config::getConfigs('product');
        
        // Set the directory to send our files and create it
        $directory = public_path("../{$config->product_image_dir}/{$product_id}/");
        createDir( $directory );
        
        // Upload top banner
        if (isset($files['imagetop'])) {
            if (!empty($files['imagetop'])) {
                $file = $directory . 'imagetop.png';
                $success = Image::make($files['imagetop'])->resize($config->banner_top_width, null, function ($constraint) { $constraint->aspectRatio(); })->save( $file );
                if ( ! $success  ) $uploaded = false;
            }
        }
        
        // Upload cover image - Above the form
        if (isset($files['imagecover'])) {
            if (!empty($files['imagecover'])) {
                $file = $directory . 'imagecover.png';
                $success = Image::make($files['imagecover'])->resize($config->image_cover_width, null, function ($constraint) { $constraint->aspectRatio(); })->save( $file );
                if ( ! $success  ) $uploaded = false;
            }
        }
        
        // Upload right banner
        if (isset($files['imageright'])) {
            if (!empty($files['imageright'])) {
                $file = $directory . 'imageright.png';
                $success = Image::make($files['imageright'])->resize($config->image_cover_width, null, function ($constraint) { $constraint->aspectRatio(); })->save( $file );
                if ( ! $success  ) $uploaded = false;
            }
        }
        
        // Return
        return $uploaded;
    }
    
    public static function deleteThis($id)
    {
        // Get configs about size of images
        $config = Config::getConfigs('product');
        
        // Set the directory to delete our files
        $directory = public_path("{$config->product_image_dir}/{$id}/");
        $delete_dir = deleteDir( $directory );
        
        // return
        return Product::destroy($id);
    }
}
