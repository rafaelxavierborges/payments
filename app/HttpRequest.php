<?php

namespace App;

use Exception;

class HttpRequest
{
    /**
     * Faz uma requisição GET com header Content/JSON.
     * @param string $url  Url a chamar via curl
     * @param array  $data Array de dados (keys and values)
     */
    public static function jsonGet($url, $data)
    {
        
    }
    
    /**
     * Faz uma requisição POST com header Content/JSON.
     * @param string $url  Url a chamar via curl
     * @param array  $data Array de dados (keys and values)
     * @return Object|Boolean
     */
    public static function jsonPost($url, $data)
    {
        try {
            $data = arrayToJson($data);

            $ch = curl_init($url);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , false);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Content-Type: application/json',
                'Content-Length: ' . strlen($data))
            );

            $result = curl_exec($ch);

            if ( ! $result  ) {
                $message = curl_error($ch);
                throw new Exception( $message );
            } else {
                return $result;
            }
        } catch (Exception $e) {
            return json_encode([
                'error' => $e->getMessage()
            ]);
        }
    }
    
    
}
