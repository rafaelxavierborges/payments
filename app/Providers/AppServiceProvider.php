<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;
use App\Configuration;
use stdClass;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
        
        $config = new stdClass;
        $config->app_name = Configuration::where('key', 'app_name')->first();
        $config->footer   = Configuration::where('key', 'footer')->first();
        
        // Pagarme
        $config->mode = Configuration::where('key', 'pagarme_mode')->first()->value;
        $config->encryption_key = Configuration::where('key', 'pagarme_'.$config->mode.'_encryption_key')->first()->value;
        $config->api_key = Configuration::where('key', 'pagarme_'.$config->mode.'_api_key')->first()->value;
        
        $app_name = ( $config->app_name ) ? $config->app_name->value : 'pago.com.br';
        $footer   = ( $config->footer ) ? $config->footer->value : 'pago.com.br';

        View::share('app_name', $app_name);
        View::share('footer', $footer);
        View::share('config', $config);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}