<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use stdClass;

class Configuration extends Model
{
    public $timestamps = false;
    
    protected $guarded = ['id'];
    
    public static function getConfigs($item)
    {
        $result = new stdClass;
        
        if ( $item == 'pagarme' ) {
            // Tests
            $result->test_api_key = Configuration::where('key', 'pagarme_test_api_key')->first()->value;
            $result->test_encryption_key = Configuration::where('key', 'pagarme_test_encryption_key')->first()->value;

            // Production
            $result->production_api_key = Configuration::where('key', 'pagarme_production_api_key')->first()->value;
            $result->production_encryption_key = Configuration::where('key', 'pagarme_production_encryption_key')->first()->value;
            
            // Pagar.me - Postback - URL de principal de retorno do sistema que será enviada para o pagar.me
            $result->postback = Configuration::where('key', 'pagarme_postback')->first()->value;
            
            // Default Postback - URLs de postback que serão utilizadas quando o sistema não encontrar nenhum postback vinculado ao produto
            $result->default_postbacks = json_decode(Configuration::where('key', 'default_postbacks')->first()->value, true);;

            // Mode
            $result->mode = Configuration::where('key', 'pagarme_mode')->first()->value;
        }
        
        if ( $item == 'product' ) {
            $result->banner_top_width   = Configuration::where('key', 'product_banner_top_width')->first()->value;
            $result->banner_right_width = Configuration::where('key', 'product_banner_right_width')->first()->value;       
            $result->image_cover_width  = Configuration::where('key', 'product_image_cover_width')->first()->value;   
            $result->product_image_dir  = Configuration::where('key', 'product_image_dir')->first()->value;   
        }
        
        return $result;
    }
}
