<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Product;

class Transaction extends Model
{
    protected $fillable = ['id', 'amount', 'payment_method', 'card_hash', 'card_id']; //PENSAR NISSO -> nova model -> CustomerCard
    
    /**
     * Save transaction info in the database.
     * @param array $transaction
     */
    public static function store($data)
    {
        $code = request()->segment(2);
        
        $product = Product::where('code', $code)->first();
                
        $transaction = array(
            'id'             => $data->tid,
            'payment_method' => $data->payment_method,
            'amount'         => round($data->amount / 100, 2),
            'product_id'     => $product->id
        );
        
        return Transaction::create( $transaction );
    }
}
