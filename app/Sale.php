<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Sale extends Model
{
    public $table = 'sales';
    
    protected $guarded = ['id'];
    
    public static function store($transaction, $customer, $data)
    {
        $sale = array(
            'product_id'     => $transaction->product_id,
            'customer_id'    => $customer->id,
            'transaction_id' => $transaction->id,
            'payment_type'   => $transaction->payment_method,
            'status'         => $data->status
        );
        
        return Sale::create( $sale );
    }
}
